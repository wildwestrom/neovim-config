if !exists('g:vscode')
    source ~/.config/nvim/basic-setting.vim 

    source ~/.config/nvim/configplugins/plugins.vim

    source ~/.config/nvim/config/color.vim
    source ~/.config/nvim/config/keymaps.vim
    source ~/.config/nvim/config/scrollbehavior.vim

    source ~/.config/nvim/configplugins/airline.vim
    source ~/.config/nvim/configplugins/autosave.vim
    source ~/.config/nvim/configplugins/coc.vim
    source ~/.config/nvim/configplugins/easymotion.vim
    source ~/.config/nvim/configplugins/indentlines.vim
    source ~/.config/nvim/configplugins/rainbowbrackets.vim
    source ~/.config/nvim/configplugins/spell.vim
    source ~/.config/nvim/configplugins/startify.vim
    source ~/.config/nvim/configplugins/vimtex.vim
endif
