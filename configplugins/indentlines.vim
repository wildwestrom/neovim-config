set conceallevel=2                                      " set this so we wont break indentation plugin

" indentLine
let g:indentLine_char = '▏'
let g:indentLine_color_gui = '#363949'

