" Airline
let g:airline_theme='solarized'
let g:airline_powerline_fonts = 1
let g:airline#themes#clean#palette = 1
let g:airline_section_warning = ''
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#buffer_min_count = 2   " show tabline only if there is more than 1 buffer
let g:airline#extensions#tabline#fnamemod = ':t'        " show only file name on tabs
let airline#extensions#vista#enabled = 1                " vista integration
